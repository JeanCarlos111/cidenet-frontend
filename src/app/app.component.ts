import { HelpersService } from './services/helpers.service';
import { Component } from '@angular/core';
import { delay } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loading: boolean = false; // loading control spinner
  title = 'CidenetFrontend';
  constructor(private helper: HelpersService) {
    this.helper.statusLoader.pipe(delay(0)).subscribe((val: boolean) => {
      this.loading = val;
    }); // subscribe to loading 
  }
}
