export interface HttpResponse {
    success: boolean,
    message: string,
    total: number,
    data: Array<any> | any
}