export interface Employee {
    firstLastName: string,
    secondLastName: string,
    firstName: string,
    otherNames: string,
    countryOfEmployment: string,
    idType: string,
    IdNumber: string,
    email: string,
    startDate: Date,
    area: string,
    state: boolean,
    registerDate: Date
}
