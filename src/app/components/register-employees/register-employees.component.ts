import { MatDialogRef } from '@angular/material/dialog';
import { HttpResponse } from './../../interfaces/httpresponse';
import { ApiService } from './../../services/api.service';
import { Helper } from './../../interfaces/helper';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelpersService } from 'src/app/services/helpers.service';

@Component({
  selector: 'app-register-employees',
  templateUrl: './register-employees.component.html',
  styleUrls: ['./register-employees.component.scss']
})
export class RegisterEmployeesComponent implements OnInit {
  form: FormGroup;
  countrys: Array<Helper>;
  documentsTypes: Array<Helper>;
  areas: Array<Helper>;
  constructor(private fb: FormBuilder, private api: ApiService, private helper: HelpersService, private dialogref: MatDialogRef<RegisterEmployeesComponent>) {
    this.countrys = [
      {
        id: "Colombia",
        value: "Colombia"
      },
      {
        id: "United States",
        value: "United States"
      }
    ];
    this.areas = [
      {
        id: "Management",
        value: "Management"
      },
      {
        id: "Financial",
        value: "Financial"
      },
      {
        id: "Infrastructure",
        value: "Infrastructure"
      },
      {
        id: "Operation",
        value: "Operation"
      },
      {
        id: "Human talent",
        value: "Human talent"
      },
      {
        id: "Various Services",
        value: "Various Services"
      },
      {
        id: "Etc.",
        value: "Etc."
      }
    ]
    this.documentsTypes = [
      {
        id: "Citizenship card",
        value: "Citizenship card"
      },
      {
        id: "Foreigner ID",
        value: "Foreigner ID"
      },
      {
        id: "Passport",
        value: "Passport"
      },
      {
        id: "Special permission",
        value: "Special permission"
      }
    ]

    this.form = this.fb.group({
      firstLastName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('[A-Za-z]+$')]],
      secondLastName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('[A-Za-z]+$')]],
      firstName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('[A-Za-z]+$')]],
      otherNames: ['', [Validators.maxLength(50),Validators.pattern('[A-Za-z]+$')]],
      countryOfEmployment: ['', Validators.required],
      idType: ['', Validators.required],
      idNumber: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Za-z0-9-]+$')]],
      email: ['', [Validators.required, Validators.maxLength(300)]],
      startDate: ['', Validators.required],
      area: ['', Validators.required],
      state: [true, Validators.required],
      registerDate: [new Date(), Validators.required],
    })
  }

  ngOnInit(): void {
  }
  get firstLastNameRequired() { return this.form.get('firstLastName') }
  get secondLastNameRequired() { return this.form.get('secondLastName') }
  get firstNameRequired() { return this.form.get('firstName') }
  get otherNamesRequired() { return this.form.get('otherNames') }
  get IdNumberRequired() { return this.form.get('idNumber') }
  get emailRequired() { return this.form.get('email') }

  saveNewEmployee(): void {
    this.helper.openSpinner();
    let newEmployee = this.form.value;

    this.api.registerEmployee(newEmployee).subscribe(
      {
        next: (res: HttpResponse) => {
          if (res.success) {
            this.helper.closeSpinner();
            this.helper.snackBarAlert(res.message, "success");
            this.dialogref.close(true)
          } else {
            this.helper.closeSpinner();
            this.helper.snackBarAlert(res.message, "error");
          }
        },
        error: (err: any) => {
          this.helper.closeSpinner();
          this.helper.snackBarAlert(err.message, "error");
        }
      }
    )
  }

  validateStartDate(startDate: Date): void {
    const today = new Date();
    const oneMonthAgo = new Date();
    oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
    if (startDate > today) {
      this.helper.snackBarAlert("The field Start Date cannot be greater than the current date.", "error");
      this.form.get("startDate")?.setValue("");
    }
    if(startDate < oneMonthAgo) {
      this.helper.snackBarAlert("The field Start Date cannot be less than one month ago.", "error");
      this.form.get("startDate")?.setValue("");
    }
  }

  generateEmail(): void {
    let firstName = this.form.get("firstName")?.value;
    let firstLastName = this.form.get("firstLastName")?.value;
    let countryOfEmployment = this.form.get("countryOfEmployment")?.value;
    if (firstName === '' || firstLastName === '' || countryOfEmployment === '') return 
    if (countryOfEmployment === "Colombia") {
      this.form.get("email")?.setValue(`${firstName.trim().toLowerCase().replace(/\s/g, '')}.${firstLastName.trim().toLowerCase().replace(/\s/g, '')}@cidenet.com.co`);
    }
    if (countryOfEmployment === "United States") {
      this.form.get("email")?.setValue(`${firstName.trim().toLowerCase().replace(/\s/g, '')}.${firstLastName.trim().toLowerCase().replace(/\s/g, '')}@cidenet.com.us`);
    }

  }

}
