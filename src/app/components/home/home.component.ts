import { EditEmployeesComponent } from './../edit-employees/edit-employees.component';
import { FormBuilder } from '@angular/forms';
import { ConfirmDialogComponent } from './../confirm-dialog/confirm-dialog.component';
import { RegisterEmployeesComponent } from './../register-employees/register-employees.component';
import { MatDialog } from '@angular/material/dialog';
import { HelpersService } from './../../services/helpers.service';
import { HttpResponse } from './../../interfaces/httpresponse';
import { Employee } from './../../interfaces/employee';
import { ApiService } from './../../services/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Helper } from 'src/app/interfaces/helper';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  total: number = 0 // Total data
  pageIndex: number = 0 // Index per page
  pageSize: number = 10 // Size per page
  pageSizeOptions: number[] = [10, 20, 40, 80, 160] // Options 
  displayedColumns: string[] = ['firstLastName', 'secondLastName', 'firstName', 'otherNames', 'countryOfEmployment', 'idType', 'IdNumber', 
  'email', 'startDate', 'area', 'state', 'registerDate', 'actions'];
  employeesData: Array<Employee>;
  countrys: Array<Helper>;
  formEmployee = this.fb.group({
    firstLastName: [''],
    secondLastName: [''],
    firstName: [''],
    otherNames: [''],
    countryOfEmployment: [''],
    idType: [''],
    IdNumber: [''],
    email: [''],
    state: [true]
  });
  pageEvent: PageEvent | undefined
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator | undefined
  dataSource = new MatTableDataSource<any>();
  constructor(private api: ApiService, private helper: HelpersService, private dialog: MatDialog, private fb: FormBuilder) { 
    this.employeesData = [];
    this.countrys = [
      {
        id: "Colombia",
        value: "Colombia"
      },
      {
        id: "United States",
        value: "United States"
      }
    ];
  }

  //Execute the function to get the inital data when component are rendered.
  ngOnInit(): void {
    this.getDataList();
  }

  //Get the data Initial.
  getDataList(event?: PageEvent) {
    this.helper.openSpinner();
    this.pageIndex = event === undefined ? 0 : event.pageIndex
    this.pageSize = event === undefined ? this.pageSize : event.pageSize

    if (event === undefined) {
      if (this.paginator !== undefined) {
        this.paginator.firstPage()
      }
    }

    let start = this.pageIndex * this.pageSize;
    let limit = this.pageSize;

    let employee: any = {
      firstLastName: this.formEmployee.get("firstLastName")?.value,
      secondLastName: this.formEmployee.get("secondLastName")?.value,
      firstName: this.formEmployee.get("firstName")?.value,
      otherNames: this.formEmployee.get("otherNames")?.value,
      countryOfEmployment: this.formEmployee.get("countryOfEmployment")?.value,
      idType: this.formEmployee.get("idType")?.value,
      IdNumber: this.formEmployee.get("IdNumber")?.value,
      email: this.formEmployee.get("email")?.value,
      state: this.formEmployee.get("state")?.value
    }

    this.api.getInformationEmployees(start, limit, employee).subscribe(
      {
        next: (res: HttpResponse) => {
          if(res.success) {
            this.helper.closeSpinner();
            this.employeesData = res.data;
            this.dataSource.data = this.employeesData;
            this.total = res.total;
          } else {
            this.helper.closeSpinner();
            this.helper.snackBarAlert(res.message, "error");
          }
        },
        error: (err: any) => {
          this.helper.closeSpinner();
          this.helper.snackBarAlert(err.message, "error")
        }
      }
    )
  }

  registerEmployee() {
    let conf = this.dialog.open(RegisterEmployeesComponent, {
      hasBackdrop: true,
      height: '600px',
      width: '400px',
      data: {
        title: 'Register Employee',
        message: ``,
        button_confirm: 'Confirm',
        button_close: 'Cancelar',
      },
    })

    conf.afterClosed().subscribe(async (item) => {
      if (item) {
        this.getDataList();
      }
    })
  }

  editEmployee(employee: Employee) {
    let conf = this.dialog.open(EditEmployeesComponent, {
      hasBackdrop: true,
      height: '600px',
      width: '400px',
      data: {
        data: employee,
        title: 'Edit Employee',
        message: ``,
        button_confirm: 'Confirm',
        button_close: 'Cancelar',
      },
    })

    conf.afterClosed().subscribe(async (item) => {
      if (item) {
        this.getDataList();
      }
    })
  }

  deleteEmployee(id: number) {
    const conf = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      height: 'auto',
      width: 'auto',
      data: {
        title: 'Delete Employee',
        message: `¿Are you sure to delete this Employee?`,
        button_confirm: 'Yes',
        button_close: 'No',
      },
    })

    conf.afterClosed().subscribe(async (result) => {
      if (result) {
       this.helper.openSpinner();
        this.api.deleteEmployee(id).subscribe(
          {
            next: (res: HttpResponse) => {
              if(res.success) {
                this.helper.closeSpinner();
                this.getDataList();
                this.helper.snackBarAlert(res.message, "success");
              } else {
                this.helper.closeSpinner();
                this.helper.snackBarAlert(res.message, "error");
              }
            },
            error: (err: any) => {
              this.helper.closeSpinner();
              this.helper.snackBarAlert(err.message, "error")
            }
          }
        )
        
      }
    })
  }
  clean() {
    this.formEmployee.get("firstLastName")?.setValue(""),
    this.formEmployee.get("secondLastName")?.setValue(""),
    this.formEmployee.get("firstName")?.setValue(""),
    this.formEmployee.get("otherNames")?.setValue(""),
    this.formEmployee.get("countryOfEmployment")?.setValue(""),
    this.formEmployee.get("idType")?.setValue(""),
    this.formEmployee.get("IdNumber")?.setValue(""),
    this.formEmployee.get("email")?.setValue(""),
    this.formEmployee.get("state")?.setValue(true)
    this.getDataList();
  }
  }

