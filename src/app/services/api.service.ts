import { Employee } from './../interfaces/employee';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API = "https://localhost:7277/api/" //API url local
  constructor(private http: HttpClient) { }

  //Service to get the inital data Employee, return a Observable
  getInformationEmployees(start: number, limit: number, employee: any): Observable<any> {
    return this.http.request('POST', `${this.API}Employees/list?start=${start}&limit=${limit}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(employee)
    });
  };

  //Service to register a new Employee, return a Observable
  registerEmployee(employee: Employee): Observable<any> {
    return this.http.request('POST', `${this.API}Employees`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(employee)
    });
  };

  //Service to edit a exist Employee, return a Observable
  editEmployee(employee: Employee, id: number): Observable<any> {
    return this.http.request('PUT', `${this.API}Employees/${id}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(employee)
    });
  };

  //Service to delete a exist Employee, return a Observable
  deleteEmployee(id: number): Observable<any> {
    return this.http.request('DELETE', `${this.API}Employees/${id}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {}
    });
  };


}
