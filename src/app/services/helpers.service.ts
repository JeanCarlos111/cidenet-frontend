import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class HelpersService {
  statusLoader: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(private snackBar: MatSnackBar) { }
  // Open a spinner loading
  openSpinner() {
    this.statusLoader.next(true);
    return false;
  }
  //Close a spinner loading
  closeSpinner() {
    this.statusLoader.next(false);
  }

  snackBarAlert(message = '¡Ha sucedido un error!', type = 'error'): void {
    this.snackBar.open(message, '×', {
      panelClass: type,
      verticalPosition: 'top',
      duration: 3000,
    });
  }

}
